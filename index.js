/* ამოცანა #1
Make a program that filters a list of strings and returns a list with only your friends name in it.
If a name has exactly 4 letters in it, you can be sure that it has to be a friend of yours! Otherwise,
you can be sure he's not...
*/

let friends = ["George", "Nick", "Tom", "Kate", "Annie"];

const filtered = friends.filter(x => x.length == 4);

console.log(filtered);

/* ამოცანა #2
Create a function that returns the sum of the two lowest positive numbers given an array of minimum 4 positive integers.
No floats or non-positive integers will be passed. For example, when an array is passed like [19, 5, 42, 2, 77],
the output should be 7.
[10, 343445353, 3453445, 3453545353453] should return 3453455 
*/

let scores = [52, 76, 14, 12, 4];

function sumScores(array){
    array.sort((a, b) => a - b);
    console.log(array[0] + array[1]);
}

sumScores(scores);